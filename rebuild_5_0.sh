#i /bin/bash

cd ~/cord/build/;
make clean-all;
cd ~/OAI_5_0/;
git checkout Ciab-5.0;
git pull;
./start.sh;
cd ~/cord/build/;
make PODCONFIG=mcord-oai-virtual.yml;
make -j4 build; make mcord-oai-test;
