#! /bin/bash

ssh_CMD=$(ps ax | grep 8080 | tail -n 2 | head -n 1 | cut -c 28- )
thread_NUM=$(ps ax | grep 8080 | tail -n 2 | head -n 1 | cut -c 2-5)
sudo kill -9 $thread_NUM
tmux new-session -d -s GUI '$ssh_CMD'
