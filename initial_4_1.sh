#!/bin/bash

# Clean CORD pre-build system 
cd ~/cord/build/
make clean-all;

# Clean CORD repository && profile
cd ~/
sudo rm -r ~/cord/ ~/cord_profile/
sudo apt-get install curl -y
rm ~/cord-bootstrap.sh
curl -o ~/cord-bootstrap.sh https://raw.githubusercontent.com/opencord/cord/cord-5.0/scripts/cord-bootstrap.sh
cd ~/
chmod +x cord-bootstrap.sh
./cord-bootstrap.sh -v
sudo apt-get remove ansible -y
sudo pip install --upgrade pip setuptools
sudo pip install ansible==2.5.2
cd ~/
sudo rm -rf OAI_4_1/
git clone https://gitlab.com/NickHoB/NTUST_OAI_CORD.git OAI_4_1
cd ~/OAI_4_1
git checkout -f Ciab-4.1;
git pull
./start.sh
#cp config-maas.yml ~/cord/build/maas/roles/maas/tasks/config-maas.yml
cd ~/cord/build/
make PODCONFIG=mcord-oai-virtual.yml config;
make -j4 build;