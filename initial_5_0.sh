#!/bin/bash

# Clean CORD pre-build system 
cd ~/cord/build/
make clean-all;
sudo rm -r ~/cord/ ~/cord_profile/
sudo apt-get install curl -y
curl -o ~/cord-bootstrap.sh https://raw.githubusercontent.com/opencord/cord/cord-5.0/scripts/cord-bootstrap.sh
cd ~/
chmod +x cord-bootstrap.sh
./cord-bootstrap.sh -v
sudo apt-get remove ansible -y
sudo pip install --upgrade pip setuptools
sudo pip install ansible==2.5.2
cd ~/
git clone https://github.com/aweimeow/oai_scenario.git OAI_5_0
cd ~/OAI_5_0
git checkout cordpod-5.0;
sed -i 's/65536/32768/g' mcord-oai-virtual.yml
./start.sh
#cp config-maas.yml ~/cord/build/maas/roles/maas/tasks/config-maas.yml
cd ~/cord/build/
make PODCONFIG=mcord-oai-virtual.yml;
make -j4 build;make mcord-oai-test;
