#!/bin/sh

#Check input
if [ -z $1 ] ; then

echo "Specify the interface you connect to the PC"
echo "Usage: ./run.sh enp4s0f1"
exit 1

fi
;;

# Add interface to leaf-spine switch

sudo ovs-vsctl add-port leaf1 $1 ;

# Add flow to onos

ssh head1 < do_onos_curl.sh ;