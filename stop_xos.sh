#! /bin/bash

cd ~/cord/build/;
make xos-teardown; make clean-openstack; make clean-onos; rm milestones/cord-config; rm milestones/copy-co*;
make -j4 build; make compute-node-refresh ; make mcord-oai-test

