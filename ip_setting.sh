#!/bin/bash

# before launching the script, check first 
# check that oaibbu has already created
# head node ip/ management network ip for each instance
# check epc ip first

# Server iptables configuration 
echo start Iptables configuration for server

git clone https://gist.github.com/7d6c9b8d0f2e8389e07f328689340813.git ip
cd ip/
# adapt iptables rule
sudo iptables-restore < Iptables_cord_server.rules

# head1 iptables configuration
ssh head1
git clone https://gist.github.com/cbfbee6e9413cd48bdc8124b3402c0ab.git ip
cd ip/
# adapt iptables rule
sudo iptables-restore < Iptables_head_node.rules
sudo modprobe nf_conntrack_proto_sctp

# add ssh key to VNF on compute node
scp ~/.ssh/id_rsa ubuntu@10.1.0.14:~/.ssh

# compute node iptables configuration
ssh ubuntu@10.1.0.14
git clone https://gist.github.com/8f10171c052f88b94b39daf7b4017e74.git ip
cd ip/
sudo iptables-restore < Iptables_compute_node.rules
sudo modprobe nf_conntrack_proto_sctp

# log in to MME instance, should check the management first
echo modify MME configuration 

#ssh ubuntu@172.27.0.2
#sudo sed -i 's/MCC="001" ; MNC="01";/MCC="208" ; MNC="93";/g' /usr/local/etc/oai/mme.conf 
#sudo sed -i 's/10.0.8.1/10.0.8.2/g' /usr/local/etc/oai/mme.conf
#sudo sed -i 's/10.0.7.1/10.0.7.2/g' /usr/local/etc/oai/freeDiameter/mme_fd.conf
