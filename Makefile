RED=\033[0;31m
NC=\033[0m
LRED=\033[1;31m
LBLUE=\033[1;34m
LPURPLE=\033[1;35m
LCYAN=\033[1;36m

help:
	@echo "${LBLUE}[Install]${NC}${LCYAN} initial-4.1${NC}             - Install CORD-4.1 and OAI service (EPC+BBU) for a fresh machine"
	@echo "${LBLUE}[Install]${NC}${LCYAN} initial-5.0${NC}             - Install CORD-5.0 and OAI service (EPC) for a fresh machine"
	@echo "${LBLUE}[Install]${NC}${LCYAN} initial-5.0-bbu${NC}         - Install CORD-5.0 and OAI service (EPC+BBU) for a fresh machine"
	@echo "${LPURPLE}[Tool]${NC}${LCYAN}    show_pwd${NC}                - Show the MCORD GUI ACCESS PASSWORD"
	@echo "${LPURPLE}[Tool]${NC}${LCYAN}    show_node${NC}               - Show nova status"
	@echo "${LRED}[Debug]${NC}${LCYAN}   rebuild-5.0${NC}             - Clean all the system and update script and synchronizer automatically"
	@echo "${LRED}[Debug]${NC}${LCYAN}   build-5.0${NC}               - Build command for 5.0 system"
	@echo "${LPURPLE}[Tool]${NC}${LCYAN}    insert_key${NC}              - Insert head node key into compute node manually"
	@echo "${LPURPLE}[Tool]${NC}${LCYAN}    Create_OpenStack_tunnel${NC} - Create dashboard link to 9999"
	@echo "${LPURPLE}[Tool]${NC}${LCYAN}    Create_ONOS_tunnel${NC}      - Create ONOS link to 8182"
	@echo "${LPURPLE}[Tool]${NC}${LCYAN}    repair_GUI${NC}              - Repair GUI tunnel after successfully building"
	@echo "${LPURPLE}[Tool]${NC}${LCYAN}    stop_xos${NC}                - Clean XOS VM / the step before you want to have any operation in Compute node"
	@echo "${LPURPLE}[Tool]${NC}${LCYAN}    ssh_compute${NC}             - Directly go to compute node"
	@echo "${LPURPLE}[Tool]${NC}${LCYAN}    auto_RRU_to_BBU${NC}         - Create connection for C-RAN scenario"


initial-4.1:
	@sh initial_4_1.sh
initial-5.0:
	@sh initial_5_0.sh
initial-5.0-bbu:
	@sh initial_5_0_bbu.sh
show_pwd:
	@ssh head1 "cat /opt/credentials/xosadmin@opencord.org"
show_node:
	@ssh head1 "source /opt/cord_profile/admin-openrc.sh;nova list --all-tenants"
insert_key:
	@ssh head1 "scp ~/.ssh/id_rsa ubuntu@10.1.0.14:~/.ssh"
rebuild-5.0:
	@sh rebuild_5_0.sh
build-5.0:
	@sh build_5_0.sh
Create_OpenStack_tunnel:
	@tmux new-session -d -s horizon 'ssh -L 0.0.0.0:9999:10.1.0.11:80 head1'
Create_ONOS_tunnel:
	@ssh -NfL 0.0.0.0:8182:0.0.0.0:8182 head1
repair_GUI:
	@sh repair_GUI.sh
stop_xos:
	@sh stop_xos.sh
ssh_compute:
	@ssh -t head1 "ssh -t ubuntu@10.1.0.14"
connect_rru_to_bbu:
	@sh create_rru_bbu_flow.sh
fix_provision:
	@ssh -t head1 "ssh -t ubuntu@10.1.0.14 "sudo pip uninstall urllib3 -y""
