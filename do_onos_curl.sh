#!/bin/sh

# Get device ID

curl --request GET  localhost:8182/onos/v1/devices --user onos:rocks >> switchName
SWITCH=`cat switchName | awk '{print $1}' | cut -d ':' -f 3- | cut -c -19 `

curl --header "Content-Type: application/json" \
--request POST \
--data '{"priority":60000,"timeout":0,"isPermanent":true,"deviceId":"of:0000525400a04bb2","treatment":{"instructions":[{"type":"OUTPUT","port":"3"}]},"selector":{"criteria":[{"type": "ETH_TYPE","ethType": "0x0800"},{"type":"IN_PORT","port":"2"},{"type": "IPV4_SRC","ip": "10.8.1.0/24"}]}}' \
localhost:8182/onos/v1/flows/${SWITCH} --user onos:rocks

curl --header "Content-Type: application/json" \
--request POST \
--data '{"priority":60000,"timeout":0,"isPermanent":true,"deviceId":"of:0000525400a04bb2","treatment":{"instructions":[{"type":"OUTPUT","port":"2"}]},"selector":{"criteria":[{"type": "ETH_TYPE","ethType": "0x0800"},{"type":"IN_PORT","port":"3"},{"type": "IPV4_DST","ip": "10.8.1.0/24"}]}}' \
localhost:8182/onos/v1/flows/${SWITCH} --user onos:rocks

